using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_rppoon
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberSequence array1 = new NumberSequence(new double[] {12.23, 5, 1, 14, 35.8694, 12, 1.23, 6, 2, 3 });
            NumberSequence array2 = new NumberSequence(new double[] { 12.23, 5, 1, 14, 35.8694, 12, 1.23, 6, 2, 3 });
            NumberSequence array3 = new NumberSequence(new double[] { 12.23, 5, 1, 14, 35.8694, 12, 1.23, 6, 2, 3 });

            Console.WriteLine(" Sequential sort: \n" + array1.ToString());
            array1.SetSortStrategy(new SequentialSort());
            array1.Sort();
            Console.WriteLine(array1.ToString());

            Console.WriteLine(" Bubble sort: \n" + array2.ToString());
            array2.SetSortStrategy(new BubbleSort());
            array2.Sort();
            Console.WriteLine(array2.ToString());

            Console.WriteLine(" Comb sort: \n" + array3.ToString());
            array3.SetSortStrategy(new CombSort());
            array3.Sort();
            Console.WriteLine(array3.ToString());
            
            //2.zad
            Console.WriteLine("Linear search: \n" + array1.ToString());
            array1.SetSearchStrategy(new LinearSearch());
            Console.WriteLine(array1.Search(6));
        }
    }
}
